<?php
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

require_once __DIR__.'/../vendor/autoload.php';
$app = new Silex\Application();
$app["debug"] = TRUE;
$app->register(new Silex\Provider\SessionServiceProvider());

require_once __DIR__.'/../vendor/classes.php';
$auth = new Auth($app);
$db = new Db($app, $auth);
$view = new View($app, $db, $auth);

require_once __DIR__.'/../vendor/db.php';

/* If user not logged in, redirect to login page */
$app->before(function ($request, $app) {
	$urlArr = parse_url($_SERVER['REQUEST_URI']);
	if($urlArr['path'] != '/' && $urlArr['path'] != '/thankyou' && $urlArr['path'] != '/createuser') {
		if (!$app['session']->get('user')) {
			return $app->redirect($app['url_generator']->generate('login'));
		}
	}
});

/* ***Login routes** */
	$app->get('/', function ($name) use ($app, $view) {
		return $view->renderPage(null, 'login.tmpl');
	})->bind('login');

	$app->get('/signout', function () use ($app) {
		$app['session']->set('user', false);
		return $app->redirect($app['url_generator']->generate('thankyou'));
	});

	$app->get('/invite/{listId}', function ($listId) use ($app, $view) {
		$selectUsersHtml = $view->getUsersListSelectHtml();
		return $view->renderPage([$listId, $selectUsersHtml], 'invite.tmpl');
	});

	$app->post('/grantaccess', function(Request $request) use($app, $auth, $db) {
		$postData = $request->request->all();
		if ( $db->grantAccess($postData['user'], $postData['list_id']) ) {
			return $app->redirect('/list/'.$postData['list_id'].'?granted=1');
		}
		return $app->redirect('/list/'.$postData['list_id'].'?notgranted=1');
	});

	$app->post('/createuser', function(Request $request) use($app, $auth, $db) {
		$userData = $request->request->all();
		//check if user by this facebook_id exists in db
	    $res = $auth->getUserByFbId($userData['id']);
		$userData['facebook_id'] = $userData['id'];
	    if ($res) { //if exists, login
	    	$userData['id'] = $res['id'];
	    	$auth->loginUser($userData);
	    } else { //otherwise insert new user into db and login
	    	$userId = $auth->createUser($userData);
	    	$userData['id'] = $userId;
	    	$auth->loginUser($userData); 
	    }
		return $app->redirect($app['url_generator']->generate('main')); 
	}); 

	$app->get('/thankyou', function () use ($app, $view) {
		return $view->renderPage(null, 'thank-you.tmpl');
	})->bind('thankyou');


/* ***Display pages** */
	$app->get('/main/{downmark}', function ($downmark, Request $request) use ($app, $view, $auth) {
		$user = $app['session']->get('user');
		$listsHtml = $view->getListsHtml($auth->getCurrentUserId());
		$qr = $request->query->all();
		$allListsHtmlTable = $view->getAllListsHtmlTable($auth->getCurrentUserId(), $qr['date-from'], $qr['date-to']);
		$photoUrl = $auth->getCurrentUserPic(); $photoUrl = $photoUrl['photo'];
		return $view->renderPage([$listsHtml, $photoUrl, $allListsHtmlTable], 'main-menu.tmpl');
	})->value('downmark', null)->bind('main');

	$app->get('/list/{listId}/{downmark}', function ($listId, $downmark, Request $request) use ($app, $view, $auth, $db) {
		$activeHtml = $view->getActiveItemsHtml($listId);
		$boughtHtml = $view->getBoughtItemsHtml($listId);
		$naHtml = $view->getNaItemsHtml($listId);
		$amountHtml = $view->getAmountHtml($listId);
		if ($db->getListAmount($listId)) {
			$amountHtml = $view->getAmountHtml($listId);
		} else {
			$amountHtml = '';
		}
		$deletedHtml = $view->getDeletedItemsHtml($listId);
		if ($allItems = count($db->getListItems($listId, 'active')) + count($db->getListItems($listId, 'na')) + count($db->getListItems($listId, 'bought')) + count($db->getListItems($listId, 'deleted'))  ) {
			$percCompletion = number_format( (1-(count($db->getListItems($listId, 'active'))/$allItems )) *100, 0);
		} else {
			$percCompletion = 0;
		}
		$qr = $request->query->all();
		$granted = '';
		if (isset($qr['granted'])) { $granted = $view->getAlertHtml('granted'); }
		if (isset($qr['notgranted'])) { $granted = $view->getAlertHtml('notgranted'); }
		$photoUrl = $auth->getCurrentUserPic(); $photoUrl = $photoUrl['photo'];
		return $view->renderPage([
				$listId, 
				$activeHtml, 
				$boughtHtml, 
				$naHtml, 
				$deletedHtml, 
				$percCompletion, 
				$db->getListTitle($listId), 
				$granted, 
				$photoUrl,
				$amountHtml
			], 'list.tmpl', $downmark);
	})->bind('list')->value('downmark', null)->before($auth->userListAuthorization());

	$app->get('/reorder/{listId}/{downmark}', function ($listId, $downmark, Request $request) use ($app, $view, $auth, $db) {
		$reorderItemsHtml = $view->getReorderItemsHtml($listId);
		$photoUrl = $auth->getCurrentUserPic(); $photoUrl = $photoUrl['photo'];
		return $view->renderPage([$listId, $photoUrl, $reorderItemsHtml], 'order.tmpl', $downmark);
	})->value('downmark', null)->before($auth->userListAuthorization());

	$app->get('/copy/{listId}', function ($listId) use ($app, $view) {
		$selectListsHtml = $view->getListSelectHtml();
		return $view->renderPage([$listId, $selectListsHtml], 'copy.tmpl');
	});

/* ***List Actions** */
	$app->post('/newlist', function (Request $request) use ($app, $view, $auth, $db) {
		$title = $request->request->get('title');
		$newListId = $db->createNewList(['title' => $title]);
		return $app->redirect('/list/'.$newListId);
	});
	$app->post('/newlistimport', function (Request $request) use ($app, $view, $auth, $db) {
		$title = $request->request->get('title');
		$items = array_map("rtrim", explode("\n", $request->request->get('list-items')));
		$newListId = $db->createNewList(['title' => $title]);
		foreach ($items as $itemName) {
			$db->createListItem($newListId, $itemName, 'active');
		}
		return $app->redirect('/list/'.$newListId);
	});
	$app->get('/deletelist/{listId}/{perm}', function ($listId, $perm, Request $request) use ($app, $view, $auth, $db) {
		$db->deleteList($listId, $perm);
		return $app->redirect($app['url_generator']->generate('main'));
	})->value('perm', false)->before($auth->userListAuthorization($listId));

	$app->post('/changelistname/{listId}', function ($listId, Request $request) use ($app, $view, $auth, $db) {
		$db->setListName($listId, $request->request->get('list_name'));
		return $app->redirect('/main');
	})->before($auth->userListAuthorization($listId));

	$app->post('/paid/{listId}', function ($listId, Request $request) use ($app, $view, $auth, $db) {
		$postData = $request->request->all();
		$db->setListAmount($listId, $postData['amount']);
		return $app->redirect('/list/'.$listId.'/downmark');
	})->before($auth->userListAuthorization($listId));

/* ***Item Actions** */
	$app->post('/newitem', function (Request $request) use ($app, $view, $auth, $db) {
		$listId = $request->request->get('list_id');
		$itemName = $request->request->get('item_name');
		$itemStatus = 'active';
		$newItemId = $db->createListItem($listId, $itemName, $itemStatus);
		return $app->redirect('/list/'.$listId.'/downmark');
	})->before($auth->userListAuthorization());
	
	$app->post('/copyitems/{listId}', function ($listId, Request $request) use ($app, $view, $auth, $db) {
		$postData = $request->request->all();
		$items = $db->getListItems($postData['source-list-id'], $postData['item-category']);
		foreach ($items as $it) {
			$newItemId = $db->createListItem($postData['dest-list-id'], $it['name'], 'active');
		}
		return $app->redirect('/list/'.$postData['dest-list-id']);
	})->before($auth->userListAuthorization());

	$app->post('/newbulkitem/{downmark}', function ($downmark, Request $request) use ($app, $view, $auth, $db) {
		$listId = $request->request->get('list_id');
		$itemName = $request->request->get('items');
		$itemStatus = 'active';
		$newItemId = $db->createListItem($listId, $itemName, $itemStatus);
		return $app->redirect('/list/'.$listId.'/downmark');
	})->value('downmark', null);

	$app->get('/changeitemstatus/{listId}/{itemId}/{status}', function ($listId, $itemId, $status, Request $request) use ($app, $view, $auth, $db) {
		$db->changeItemStatus($itemId, $status);
		return $app->redirect('/list/'.$listId.'/downmark');
	})->before($auth->userListAuthorization($listId));

	$app->get('/cleardeleted/{listId}', function ($listId, Request $request) use ($app, $view, $auth, $db) {
		$db->clearDeletedItems($listId);
		return $app->redirect('/list/'.$listId);
	})->before($auth->userListAuthorization($listId));;

	$app->post('/setitemorder/{listId}', function ($listId, Request $request) use ($app, $view, $auth, $db) {
		foreach ($request->request->all() as $itemId => $order){
			$db->setItemOrder($itemId, $order);
		}
		return $app->redirect('/list/'.$listId);
	})->before($auth->userListAuthorization($listId));

	$app->post('/changeitemname/{listId}/{itemId}', function ($listId, $itemId, Request $request) use ($app, $view, $auth, $db) {
		$db->setItemName($itemId, $request->request->get('item_name'));
		return $app->redirect('/list/'.$listId);
	})->before($auth->userListAuthorization($listId));

$app->run();
