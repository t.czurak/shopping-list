function reloadView(endpoint) {
	var s = $("article").scrollTop();
	if (!$('.item-input').is(':focus') && !$('#collapse-menu').is(':visible') && !$('#spinner').is(':visible')) {
		$.get( window.location.href+"/downmark", function( data ) {
			$( 'body' ).html(data);
			$("article").scrollTop(s);
		});
	}
};